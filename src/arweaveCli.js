#!/usr/bin/env node
import arg from 'arg';
import inquirer from 'inquirer';
import axios from 'axios';
import fs from 'fs';
import {execSetArweaveJson} from './processCommands.js';

function parseArguments(rawArgs) {
 const args = arg(
   {
     '--install': Boolean,     
     '--set': Boolean,
     '--get': Boolean,     
   },
   {
     argv: rawArgs.slice(2),
   }
 );
 return {        
   set: args['--set'] || false,
   get: args['--get'] || false,
   template: args._[0],
   wallet: args._[1],
   url: '',
   counter: 0,
   msg: 'none',
   status: true,
 };
}

function validateArguments(options) {
    const defaultTemplate = 'JavaScript';
    if (!options.set && !options.get) {
        console.log('error: --set or --get arguments are not set');
        console.log('node src/arweaveCli.js --set 12:https://arweave.net/{Tran ID}/?ext=json {path_to_arweave_wallet}');
        options.status = false;
        return {
        ...options,        
      };
    }

    let setParam = options.template;

    //if the param is not in this format '12:https://arweave.net/{Tran ID}/?ext=json' then throw an error
    if(setParam.indexOf(":") < 0){
        console.log("The set argument is not in proper format. It should be '12:https://arweave.net/{Tran ID}/?ext=json'");
        options.status = false;
        return options;
    }

    let paramArray = setParam.split(":");
    
    let counter = paramArray[0];
    options.counter = counter;
    //validation to see if Meditation Days is a number
    let meditationCounter = parseInt(counter);
    if (isNaN(meditationCounter)) {
        console.log("the counter is not set in the set parameter");
        options.status = false;
        return options;
    }

    let url = '';
    if(paramArray.length == 3){
        url = paramArray[1].concat(":", paramArray[2]);
    }else if(paramArray.length == 2){
        url = 'https://'.concat(paramArray[1]);
    }
    
    options.url = url;
    try {
        new URL(url);        
        //console.log('good url');        
    } catch (err) {
        console.log(err);
        options.status = false;
        return {
            ...options,            
          };
    }
    //if(url.startsWith(''))

    let wallet = options.wallet.trim();    
    if (fs.existsSync(`${wallet}`)){        
        //console.log("wallet file exists");
    }else{
        console.error('wallet does not exist');
        options.status = false;
    }

    return{
        ...options,
    }
          
}


//export function cli(args) {
async function cli(args) {
    //console.log(args);
    let options = parseArguments(args);

    //validate the arguments
    options = validateArguments(options);
    
    //check for status
    if(!options.status){
        return;
    }

    if(options.set){
        //--set option is invoked
        execSetArweaveJson(options)        
    }
    else{
        //--get option is invoked
        console.log('get selected');
    }
}

    //main process
    cli(process.argv);

    //end of main    