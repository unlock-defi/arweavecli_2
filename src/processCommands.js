import axios from 'axios';
import fs from 'fs';
import { ArweaveSigner, bundleAndSignData, createData} from 'arbundles';
//import './cliConfig.js';

const BUNDLER_GATEWAY = "https://node1.bundlr.network";


export async function execSetArweaveJson(options){
    //let url = 'https://arweave.net/1jf5c8EGbuQmJeh_7R4CPJg3syva1WBwKgDJsE2Kzcg/?ext=json'
    let url = options.url;
    let json = null;
    let updateArFlag = false;

    //load the wallet
    let wallet = options.wallet;
    let arwsigner = loadArweaveWallet(wallet);
    
    axios.get(url)
    .then(response=>{
        //console.log('making http call');        
        json = JSON.parse(JSON.stringify(response.data));
        //console.log(json.name);        
        let length = json.attributes.length;
        for(let i=0; i < length; i++){
            if(json.attributes[i].trait_type == 'MEDITATION DAYS'){
                json.attributes[i].value = options.counter;
                updateArFlag = true;
                break;
            }
        }
        
        if(!updateArFlag)
            return;

        sendFileToBundler(json, arwsigner).then(response => {
            console.log(`${response.data.id}`);
            return response;
        });             

    })
    .catch(error=>{
        console.log(`error:${error}`);        
    })

    //console.log(json);

}

async function sendFileToBundler(json, arwsigner){

    let jsonTags = [
        {name: 'Content-Type', value: `application/json`}
    ];
    
    let dataStr = JSON.stringify(json, null, 2); 
    //the following command helps align the json values into the format that Arweave expects
    let optTags = jsonTags.slice();
    //optTags.push({name:'File-Name', value: `${fileName}.${fileExt}`});     
    
    let item = createData(dataStr, arwsigner, {tags: optTags});

    await item.sign(arwsigner);
    let txId = null;
    let response = await item.sendToBundler(BUNDLER_GATEWAY);
    if( response != null){
        if(response.status == 200){
            txId = response.data.id;
            //logger.log('info', `tx id for file name ${fileNameNoExt}.${MP4_FILE_EXT}: ${response.data.id}`);
            //console.log(`file is uploaded to arweave ${response.data.id}`);                        
            return response;
        }else{
            console.log(`response status returned ${response.status}`);
        }
    }else{
        //logger.error('error', `file ${fileNameNoExt}.${MP4_FILE_EXT} could not be uploaded`);
        console.log('file could not be uploaded');
    }

    //console.log(response.data.id);    
    //return response;
}

//loads the wallet keys from the file
function loadArweaveWallet(walletPath){
    let arwsigner = null;

    if (fs.existsSync(`${walletPath}`)){
        //console.log("wallet file exists");
        const jwk = JSON.parse(
            fs.readFileSync(`${walletPath}`, {encoding: 'utf8'})
        )
        arwsigner = new ArweaveSigner(jwk);
    }else{
        console.log('wallet does not exist');
    }
    return arwsigner;
}