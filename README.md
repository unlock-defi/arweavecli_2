# Arweave CLI README #

-This README file contains the steps necessary to run the Arweave CLI.

### What is Arweave CLI? ###

* Arweave CLI is a tool that takes command line arguments for an already existing json file, updates the 'Counter' attribute in the json file and then pushes the updated json file into Arweave Permaweb.
* Version - 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
	- npm and yarn.
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact